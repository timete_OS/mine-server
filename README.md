mines server
=====================

I need a backend for my mine sweeper that I won't pay for while nobody is using it.

I like the idea of small modular componant for a web service, that only loads what it needs to when responding to a request.

That is why I tried to use Spin, to deploy small WASI components for free (as long as no one is calling them). The only cost is basically to store components that are a few MB at most.


## TO DO

- Update the db with the new tokens
- Fallback on the DB if the key-value store does not have the information
- Decide to keep or not the current db schema