use anyhow::{Ok, Result};
use serde_json;
use commun::{PublicUser, User};
use spin_sdk::{
    http::{IntoResponse, Request, Response},
    http_component,
    sqlite::Connection,
};

fn retrieve_users(conn: Connection) -> Result<Vec<User>> {
    let rowset = conn.execute(
        "SELECT * FROM user LIMIT 10000",
        &[]
    )?;

    let matched_users = rowset.rows().filter_map(|row| User::from_db(row)).collect();
    Ok(matched_users)
}

/// A simple Spin HTTP component.
#[http_component]
fn all_users(_req: Request) -> anyhow::Result<impl IntoResponse> {
    println!("return (almost) all users...");

    let connection = Connection::open_default()?;
    
    let users = retrieve_users(connection)?;
    let public_users: Vec<PublicUser> = users.into_iter().map(|u| PublicUser::from(u)).collect();
    let body = serde_json::to_string(&public_users)?;    

    Ok(Response::builder()
        .status(200)
        .header("content-type", "application/json")
        .body(body)
        .build())
}
