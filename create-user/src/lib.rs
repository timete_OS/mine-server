use commun::{Params, PublicUser, User, insert_token};
use spin_sdk::{
    http::{IntoResponse, Request, Response, Method},
    http_component,
    key_value::Store,
    sqlite::Connection,
};
use uuid::Uuid;

/// A simple Spin HTTP component.
#[http_component]
fn create_user(req: Request) -> anyhow::Result<impl IntoResponse> {
    println!("attempt to create a user...");

    println!("the method is {}", req.method());
    let default_response = Ok(Response::builder()
    .status(416)
    .header("content-type", "text/plain")
    .build());

    match req.method() {
        Method::Put => (),
        _ => return default_response
    }

    let mut user = match User::from_bytes(req.body()) {
        Ok(u) => u,
        _ => return default_response
    };

    println!("{:?}", user);

    let store = Store::open_default()?;
    let connection = Connection::open_default()?;

    if user.insert_into_db(&connection)? {
        println!("{:?}", user);
    } else {
        return Ok(Response::builder()
        .status(401)
        .header("content-type", "text/plain")
        .body(format!("username {} is already taken", user.name))
        .build())      
    }

    let token = Uuid::new_v4().to_string();
    let body = serde_json::to_string(&Params {
        user_id: user.id,
        token_value: token.clone(),
    })?;

    insert_token(token.clone(), user.id, &connection)?;
  
    let raw_user = PublicUser::from(user).to_bytes();
    store.set(token.as_str(), raw_user.as_ref())?;

    Ok(Response::builder()
        .status(200)
        .header("content-type", "application/json")
        .body(body)
        .build())
}
