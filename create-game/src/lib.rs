use commun::{Game, PublicUser, User};
use base64::{engine::general_purpose, Engine as _};
use konst::iter;

use spin_sdk::{
    http::{IntoResponse, Request, Response, Method},
    http_component,
    key_value::Store,
    sqlite::Connection,
    variables
};

const PERMUTATION_BOX: [u8; 256] = [255, 2, 3, 100,  64, 228,  34, 214, 233,  94, 108, 114, 152,
71,  40,   123,  83, 150, 120, 177,  68, 176, 209, 170, 200,  91,
127, 119, 153,   9, 215, 174,  44,  53, 131, 187,  48, 185, 102,
82, 208, 230, 243,  29, 201,  45,   6,  62,  38, 135,  63, 222,
219,  86,  32, 178, 159,  93,  31,  28,  67, 139, 193, 164,   4,
84, 229, 205, 239,  66,  36, 191,  58, 148,  79, 195, 107,  24,
138, 251,  98,  27, 121,  20, 105, 236,  77, 155,  26, 145,  51,
39, 190, 168, 126, 237, 223, 157, 154,  75, 244, 172, 192, 133,
122, 202,  19,  33, 130, 181, 216, 169, 87, 249,  80,  54, 204,
74,  70,  96, 188, 171, 218, 101, 182, 186, 203,  52,  95,  76,
254, 207, 147,   8,  42,  65,  23, 140,   7, 211, 194, 158,   248,
16,  41, 212,  78,  22,  61, 180, 104,   163,  50,  15, 240, 117,
128, 247, 189, 166, 206, 134,  47,  37, 132, 198, 160, 226, 115,
231, 253, 141, 109, 111,  55, 161,  10, 146,  13,  59,  60,  25,
99,  17,  43, 110, 199, 238, 245, 136,  85, 224, 234, 142, 235,
173, 210,  69,  81,  18,  89, 103, 225, 184, 232,  73, 183, 241,
11, 113, 246,   5, 116, 162, 197,  12,  49, 118, 143, 129,  57,
90,  14, 165, 124, 156,   1, 112,  35,  21, 137, 217,  97, 149,
175, 106,  92, 144, 125, 179, 242,  88, 196,  30,  46, 252, 151,
227, 167,  56, 250,  72, 220, 221, 213, 0];

const fn inv(x: u8) -> u8 {
    let mut i = 0;
    while PERMUTATION_BOX[i] != x {
        i += 1;
    }
    i as u8
}

const INV_PERMUTATION_BOX: [u8; 256] = iter::collect_const!(u8 => 0..=255, map(inv));

fn dechiffre(cipher: &[u8], key: &[u8]) -> Vec<u8> {
    let shift_box: Vec<u8> = (0..=255).map(|x: u8| x.overflowing_sub(7).0).collect();

    let mut block = cipher.to_owned();
    for _ in 0..16 {
        block.iter_mut().zip(key.iter()).for_each(|(x, y)| *x = INV_PERMUTATION_BOX[*x as usize] ^ y);
        block.iter_mut().for_each(|x| *x = shift_box[*x as usize]);
    }

    block
}

/// A simple Spin HTTP component.
#[http_component]
fn handle_create_game(req: Request) -> anyhow::Result<impl IntoResponse> {
    println!("attempt to create a game...");

    println!("the method is {}", req.method());

    let default_response = Ok(Response::builder()
    .status(416)
    .header("content-type", "text/plain")
    .build());

    let forbidden = Ok(Response::builder()
    .status(401)
    .header("content-type", "text/plain")
    .build());


    match req.method() {
        Method::Put => (),
        _ => return default_response
    }

    let token = match req.header("token") {
        Some(t) => t.as_str().unwrap(),
        None => return forbidden
    };

    let passcode = match req.header("passcode") // return an Option
        .and_then(|t| t.as_str()) // return an Option too
        .and_then(|t| Some(general_purpose::STANDARD.decode(t))) {
            Some(p) if p.is_ok() => p.unwrap(),
            _ => return forbidden
        };

    let interface_key = variables::get("interface_key").expect("could not get variable");
    let res = dechiffre(&passcode, interface_key.as_bytes());
    let valid = token[..16].as_bytes().iter().zip(res[..16].iter()).fold(true, |b, (x, y)| b & (x == y));
    if valid {
        println!("Passcode is valid, insertion can proceed.");
    } else {
        return forbidden;
    }

    let mut game = match Game::from_bytes(req.body()) {
        Ok(u) => u,
        _ => return default_response
    };

    let store = Store::open_default()?;
    let connection = Connection::open_default()?;

    // get user if valid
    let mut user = match store.get(token)?.and_then(|bytes| Some(User::from_bytes(&bytes)))
    {
        Some(user) if user.is_ok() => user.unwrap(),
        _ => {
            match User::get_from_db(&connection, game.player_id)? {
                Some(u) => u,
                None => {
                    println!("user is not in the db");
                    return forbidden
                }
            }
        }
    };

    game.insert_into_db(&connection)?;
    if game.time < user.best_time{
        user.best_time = game.time;
        user.update(&connection)?;
        let raw = PublicUser::from(user).to_bytes();
        store.set(token, raw.as_ref())?;
    }

    println!("Game inserted into db.");

    let body = serde_json::to_string(&game)?;

    Ok(Response::builder()
        .status(200)
        .header("content-type", "application/json")
        .body(body)
        .build())
}
