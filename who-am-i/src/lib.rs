use commun::{PublicUser, User};
use serde_json;
use spin_sdk::{
    http::{IntoResponse, Request, Response,},
    http_component,
    key_value::Store,
};

#[http_component]
fn handle_who_am_i(req: Request) -> anyhow::Result<impl IntoResponse> {
    println!("Handling request to {:?}", req.header("spin-full-url"));

    let forbidden = Ok(Response::builder()
    .status(401)
    .header("content-type", "text/plain")
    .build());

    let token = match req.header("token") {
        Some(t) => t.as_str().unwrap(),
        None => return forbidden
    };

    let not_found = Response::builder()
    .status(404)
    .header("content-type", "text/plain")
    .body("No user associated with this token")
    .build();

    let store = Store::open_default()?;

    match store.get(token)?.and_then(|bytes| Some(User::from_bytes(&bytes))) {
        Some(user) if user.is_ok() => {
            let body = serde_json::to_string(&PublicUser::from(user.unwrap()))?;    

            Ok(Response::builder()
                .status(200)
                .header("content-type", "application/json")
                .body(body)
                .build())
        }
        _ => Ok(not_found)
    }
}
