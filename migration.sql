CREATE TABLE IF NOT EXISTS user 
    (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL,
        member_since DATE NOT NULL,
        best_time TIMESTAMP NOT NULL,
        hashed_pswd TEXT NOT NULL
    );

CREATE TABLE IF NOT EXISTS game
    (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        date TIMESTAMP NOT NULL,
        player_id INTEGER NOT NULL,
        time TIMESTAMP NOT NULL,
        mines_number INTEGER NOT NULL,
        FOREIGN KEY (player_id) REFERENCES user(id)
    );

CREATE TABLE IF NOT EXISTS token
    (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        user_id INTEGER NOT NULL,
        token_value TEXT NOT NULL,
        creation_date TIMESTAMP NOT NULL,
        FOREIGN KEY (user_id) REFERENCES user(id)
    );