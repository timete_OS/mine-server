use std::time::Duration;
use anyhow::Result;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDate, NaiveDateTime, NaiveTime, TimeDelta};
use spin_sdk::sqlite::{Connection, Error, Row, Value};
use sha2::{Sha256, Digest};


fn datetime_to_u64(t: NaiveDateTime) -> u64 {
    let x = NaiveDate::from_ymd_opt(1970, 1, 1).unwrap();
    let y = NaiveTime::from_hms_milli_opt(0, 0, 0, 0).unwrap();        
    let origin = NaiveDateTime::new(x, y);
    (t - origin).num_seconds() as u64
}

fn u64_to_datetime(t: u64) -> NaiveDateTime {
    let x = NaiveDate::from_ymd_opt(1970, 1, 1).unwrap();
    let y = NaiveTime::from_hms_milli_opt(0, 0, 0, 0).unwrap();        
    let origin = NaiveDateTime::new(x, y);
    let delta = TimeDelta::from_std(Duration::from_secs(t)).unwrap();
    origin.checked_add_signed(delta).unwrap()
}

#[derive(Clone,PartialEq, Debug, Serialize)]
pub struct Params {
    pub user_id: usize,
    pub token_value: String,
}

#[derive(Deserialize)]
struct CreateUser {
    name: String,
    pswd: String,
    time: u64
}

#[derive(Clone, Debug, Deserialize)]
pub struct User {
    pub id: usize,
    pub name: String,
    pub member_since: NaiveDate,
    pub best_time: u64,
    pub hashed_pswd: String
}

impl User {

    pub fn get_from_db(connection: &Connection, id: usize) -> Result<Option<User>> {
        println!("searching user with id {id} in the db");
        let res = connection.execute(
            "SELECT * FROM user WHERE id=(?)",
            &[Value::Integer(id as i64)])?;
        
        let user = res.rows().next().and_then(|row| User::from_db(row));
        Ok(user)
    }

    pub fn update(&self, connection: &Connection) -> Result<()> {
        println!("Updating user {} with id {}", self.name, self.id);
        connection.execute(
            "UPDATE user SET name=(?), hashed_pswd=(?), best_time=(?) WHERE id=(?)",
            &[
                Value::Text(self.name.to_owned()),
                Value::Text(self.hashed_pswd.to_owned()),
                Value::Text(u64_to_datetime(self.best_time).to_string()),
                Value::Integer(self.id as i64)]
            )?;
        Ok(())
    }

    pub fn from_db(row: Row) -> Option<User> {
        let best_time = match row.get::<&str>("best_time") {
            Some(t) => {
                let nt = NaiveDateTime::parse_from_str(&t, "%Y-%m-%d %H:%M:%S%.f").expect("The db should store timedeltas as unix dates");
                Some(datetime_to_u64(nt))
            }
            None => {
                println!("Some user do not have a best time (or it is not a string)");
                return  None
            }
        };
        // 1970-01-01 00:24:32.000000

        let since = row.get::<&str>("member_since").unwrap().to_owned();
        let a = NaiveDate::parse_from_str(&since, "%Y-%m-%d").expect("The db date format should be %Y-%m-%d");

        Some(User {
            id: row.get::<usize>("id").unwrap(),
            name: row.get::<&str>("name").unwrap().to_string(),
            hashed_pswd: row.get::<&str>("hashed_pswd").unwrap().to_string(),
            best_time: best_time.expect("users should always have a best time"),
            member_since: a
        })
    }

    pub fn from_bytes(bytes: &[u8]) -> Result<User, serde_json::Error> {
        let created_user: CreateUser = serde_json::from_slice(bytes)?;    
        let hpswd = format!("{:X}", Sha256::digest(created_user.pswd)).to_lowercase();

        let today = chrono::offset::Local::now().date_naive();
        Ok(User {
            id: 0,
            name: created_user.name,
            hashed_pswd: hpswd,
            best_time: created_user.time,
            member_since: today
        })
    }

    pub fn insert_into_db(&mut self, conn: &Connection) -> Result<bool, Error> {

        // first check the name is available
        if conn.execute(
            "SELECT id FROM user WHERE name=(?)",
            &[Value::Text(self.name.clone())])?
            .rows
            .len() > 0 {
                println!("{} already in DB !!!", self.name);
                return Ok(false)
        }

        let execute_params = [
            Value::Text(self.name.to_owned()),
            Value::Text(self.hashed_pswd.to_owned()),
            Value::Text(u64_to_datetime(self.best_time).to_string()),
            Value::Text(self.member_since.to_string())
        ];

        // RETUNRNING statement only available on SQLite and Postgres
        let mut user_id: Vec<usize> = conn.execute(
            "INSERT INTO user (name, hashed_pswd, best_time, member_since) VALUES (?, ?, ?, ?) RETURNING *",
            execute_params.as_slice()
        )?
        .rows()
        .map(|v| v.get::<usize>("id").unwrap())
        .collect();
        
        self.id = user_id.pop().expect("user is supposed to be in the db");
        
        Ok(true)
    }
}

#[derive(Serialize, Debug)]
pub struct PublicUser {
    pub id: usize,
    pub name: String,
    member_since: NaiveDate,
    best_time: u64,
}

impl PublicUser {
    pub fn from(user: User) -> PublicUser {
        PublicUser {
            id: user.id,
            name: user.name,
            member_since: user.member_since,
            best_time: user.best_time
        }
    }

    pub fn to_bytes(self) -> Vec<u8> {
        bincode::serialize(&self).unwrap()
    }
}


#[derive(Deserialize, Debug)]
struct GameCreate {
    pub player_id: usize,
    pub time: u64,
    pub mines_number: usize
}

#[derive(Serialize, Debug)]
pub struct Game {
    id: usize,
    date: NaiveDate,
    pub player_id: usize,
    pub time: u64,
    pub mines_number: usize
}

impl Game {
    pub fn from_db(row: Row) -> Game {
        let since = row.get::<&str>("date").unwrap();
        let time = row.get::<&str>("time").unwrap();
        let t = NaiveDateTime::parse_from_str(&time, "%Y-%m-%d %H:%M:%S%.f").expect("The db date format should be %Y-%m-%d");
        
        Game {
            id: row.get::<usize>("id").unwrap(),
            date: NaiveDate::parse_from_str(since, "%Y-%m-%d").unwrap(),
            player_id: row.get::<usize>("player_id").unwrap(),
            time: datetime_to_u64(t),
            mines_number: row.get::<usize>("mines_number").unwrap()
        }
    }

    pub fn from_bytes(bytes: &[u8]) -> Result<Game, serde_json::Error> {
        let game_create: GameCreate = serde_json::from_slice(bytes)?;
        let today = chrono::offset::Local::now().date_naive();

        Ok(Game {
            id: 0,
            date: today,
            player_id: game_create.player_id,
            time: game_create.time,
            mines_number: game_create.mines_number
        })
    }

    pub fn insert_into_db(&mut self, conn: &Connection) -> Result<(), Error> {
        let time = u64_to_datetime(self.time);
        let execute_params = [
            Value::Integer(self.player_id as i64),
            Value::Text(self.date.to_string()),
            Value::Text(time.to_string()),
            Value::Integer(self.mines_number as i64)
        ];

        // RETUNRNING statement only available on SQLite and Postgres
        let mut game_id: Vec<usize> = conn.execute(
            "INSERT INTO game (player_id, date, time, mines_number) VALUES (?, ?, ?, ?) RETURNING *",
            execute_params.as_slice()
        )?
        .rows()
        .map(|v| v.get::<usize>("id").unwrap())
        .collect();
        
        self.id = game_id.pop().expect("game is supposed to be in the db");
        
        Ok(())
    }
}


pub fn insert_token(token: String, user_id: usize, conn: &Connection) -> Result<()> {
    let now = chrono::offset::Local::now().naive_local();
    conn.execute(
        "INSERT INTO token (user_id, token_value, creation_date) VALUES (?, ?, ?)",
        &[Value::Integer(user_id as i64), Value::Text(token), Value::Text(now.to_string())]
    )?;

    Ok(())
}
