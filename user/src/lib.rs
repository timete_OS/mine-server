use commun::{PublicUser, User};
use anyhow::Result;
use serde_json;
use spin_sdk::{
    http::{IntoResponse, Request, Response},
    http_component,
    sqlite::{Connection, Value},
};


fn check_database(id: usize, conn: Connection) -> Result<Option<User>> {
    let rowset = conn.execute(
        "SELECT * FROM user WHERE id=(?)",
        &[Value::Text(id.to_string())]
    )?;

    let mut matched_users: Vec<User> = rowset.rows().filter_map(|row| User::from_db(row)).collect();
    Ok(matched_users.pop())
}

#[http_component]
fn handle_user(req: Request) -> anyhow::Result<impl IntoResponse> {

    for (name, value) in req.headers() {
        println!("{name} : {}", value.as_str().unwrap());
    }

    let default_response = Ok(Response::builder()
    .status(422)
    .header("content-type", "text/plain")
    .build());

    // req.header("spin-path-info") is an option on some header value that can be parsed as &str
    let user_id: usize = if let Some(h) = req.header("spin-path-info") {
        match h.as_str().expect("should be utf-8 parsable")[1..].parse::<usize>() {
            Ok(parsed) => parsed,
            Err(_) => return default_response
        }
    } else {
        return default_response
    };

    println!("user_id : {user_id}");

    let connection = Connection::open_default()?;
    
    let user = match check_database(user_id, connection)? {
        Some(u) => u,
        _ => return Ok(Response::builder()
        .status(401)
        .header("content-type", "text/plain")
        .build()),
    };

    let res = serde_json::to_string(&PublicUser::from(user))?;    

    Ok(Response::builder()
        .status(200)
        .header("content-type", "application/json")
        .body(res)
        .build())
}
