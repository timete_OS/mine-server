use anyhow::{Ok, Result};
use uuid::Uuid;
use sha2::{Sha256, Digest};
// use hex_literal::hex;
use serde_json;
use commun::{PublicUser, User, Params, insert_token};
use spin_sdk::{
    http::{IntoResponse, Request, Response},
    http_component,
    key_value::Store,
    sqlite::{Connection, Value},
};


fn check_database(name: &str, conn: &Connection) -> Result<Option<User>> {
    let rowset = conn.execute(
        "SELECT * FROM user WHERE name=(?)",
        &[Value::Text(name.to_string())]
    )?;

    // println!("{:?}", rowset);

    let mut matched_users: Vec<User> = rowset.rows().filter_map(|row| User::from_db(row)).collect();
    // println!("{:?}", matched_users);

    Ok(matched_users.pop())
}


/// A simple Spin HTTP component.
#[http_component]
fn handle_login(req: Request) -> anyhow::Result<impl IntoResponse> {
    println!("handle login...");
    let (name, password) = match (req.header("name"), req.header("pswd")) {
        (Some(n), Some(p)) => (n.as_str().unwrap(), p.as_str().unwrap()),
        _ => return Ok(Response::builder()
        .status(422)
        .header("content-type", "text/plain")
        .build())
    };

    let store = Store::open_default()?;
    let connection = Connection::open_default()?;
    let hpaswd = format!("{:X}", Sha256::digest(password)).to_lowercase();
    
    let user = match check_database(name, &connection)? {
        Some(u) if u.hashed_pswd == hpaswd => u,
        _ => return Ok(Response::builder()
        .status(401)
        .header("content-type", "text/plain")
        .build()),
    };

    // println!("{:?}", user);

    let token = Uuid::new_v4().to_string();

    let body = Params {
        user_id: user.id,
        token_value: token.clone(),
    };

    insert_token(token.clone(), user.id, &connection)?;

    let j = serde_json::to_string(&body)?;    
    let raw = PublicUser::from(user).to_bytes();

    // set user in cache
    store.set(token.as_str(), raw.as_ref())?;


    Ok(Response::builder()
        .status(200)
        .header("content-type", "application/json")
        .body(j)
        .build())
}
